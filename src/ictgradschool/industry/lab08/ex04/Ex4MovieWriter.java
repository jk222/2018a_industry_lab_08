package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter

        try (PrintWriter writer = new PrintWriter(new FileWriter(fileName))) {
            int count = 0;

            while (count < films.length) {
                writer.println(films[count].getName() + ", " + films[count].getYear() + ", " + films[count].getLengthInMinutes() + ", " + films[count].getDirector());
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
