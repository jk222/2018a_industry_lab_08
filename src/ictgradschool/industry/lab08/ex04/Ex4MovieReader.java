package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner

        File testingFile = new File(fileName);

        try (Scanner scanner = new Scanner(testingFile)) {
            while (scanner.hasNextLine()){
                System.out.println(scanner.nextLine());

            }

        }catch (IOException e){
            System.out.println("Error: " + e.getMessage());
        }




        return new  Movie[19];
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
