package ictgradschool.industry.lab08.ex01;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {


    public void start() {

        //fileReaderEx01();

        //printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }


    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        try {
            FileReader f1 = new FileReader(new File("input2.txt"));
            int charLetter = 0;
            while ((charLetter = f1.read()) != -1) {

                total++;
                if (charLetter == 69 || charLetter == 101) {
                    numE++;
                }

            }
            f1.close();

        } catch (IOException e) {
            System.out.println("IO Problem");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }


    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.
        int charLetterRead = 0;


        File input2File = new File("input2.txt");
        try (BufferedReader reader = new BufferedReader(new FileReader(input2File))) {
            while ((charLetterRead = reader.read()) != -1) {
                total++;
                if (charLetterRead == 69 || charLetterRead == 101) {
                    numE++;
                }
            }

        } catch (IOException e) {
            System.out.println("IO Problem");
        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }


    public static void main(String[] args) {
        new ExerciseOne().start();
    }


    private void fileReaderEx01() {

        int num = 0;
        FileReader fR = null;
        try {
            fR = new FileReader("input1.txt");
            num = fR.read();
            System.out.println((char) num);
            System.out.println(((char) fR.read()));
            System.out.println(((char) fR.read()));
            System.out.println(((char) fR.read()));
            System.out.println(((char) fR.read()));
            fR.close();

        } catch (IOException e) {
            System.out.println("IO Problem");
        }
    }
}
